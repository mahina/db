BEGIN;

CREATE TABLE entity.audit(
    entity_uuid     varchar(64),
    entity_type     varchar(64),
    user_uuid       varchar(64),
    auth_token      varchar(512),
    ip_address      varchar(40),
    action          varchar(16) NOT NULL CHECK (action IN ('insert','delete','update','access','search')),
    difference      text,
    timestamp       timestamptz NOT NULL DEFAULT now()
);

CREATE INDEX entity_audit_entity_uuid_index ON entity.audit(entity_uuid);
CREATE INDEX entity_audit_entity_type_index ON entity.audit(entity_type);
CREATE INDEX entity_audit_timestamp_index ON entity.audit(timestamp);

CREATE TABLE market.audit(
    entity_uuid     varchar(64),
    entity_type     varchar(64),
    user_uuid       varchar(64),
    auth_token      varchar(512),
    ip_address      varchar(40),
    action          varchar(16) NOT NULL CHECK (action IN ('insert','delete','update','access','search')),
    difference      text,
    timestamp       timestamptz NOT NULL DEFAULT now()
);

CREATE INDEX audit_entity_uuid_index ON market.audit(entity_uuid);
CREATE INDEX audit_entity_type_index ON market.audit(entity_type);
CREATE INDEX audit_timestamp_index ON market.audit(timestamp);

COMMIT;