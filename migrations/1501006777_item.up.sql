BEGIN;

CREATE TABLE market.item_category(
    category_id text PRIMARY KEY,
    parent_id text, 
    label varchar(48), 
    node_path ltree
);
CREATE UNIQUE INDEX item_category_node_path_btree_index ON market.item_category USING btree(node_path);
CREATE INDEX item_category_node_path_gist_index ON market.item_category USING gist(node_path);

CREATE OR REPLACE FUNCTION get_calculated_item_category_node_path(param_item_category_id text)
  RETURNS ltree AS
$$
SELECT CASE WHEN s.parent_id IS NULL THEN s.category_id::text::ltree 
            ELSE get_calculated_item_category_node_path(s.parent_id)  || s.category_id::text END
    FROM market.item_category AS s
    WHERE s.category_id = $1;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION trigger_update_item_category_node_path() RETURNS trigger AS
$$
BEGIN
  IF TG_OP = 'UPDATE' THEN
        IF (COALESCE(OLD.parent_id,0) != COALESCE(NEW.parent_id,0) OR NEW.category_id != OLD.category_id) THEN
            -- update all nodes that are children of this one including this one
            UPDATE market.item_category SET node_path = get_calculated_item_category_node_path(category_id) 
                WHERE OLD.node_path  @> market.item_category.node_path;
        END IF;
  ELSIF TG_OP = 'INSERT' THEN
        UPDATE market.item_category SET node_path = get_calculated_item_category_node_path(NEW.category_id) WHERE market.item_category.category_id = NEW.category_id;
  END IF;
  
  RETURN NEW;
END
$$
LANGUAGE 'plpgsql' VOLATILE;

CREATE TRIGGER trigger_update_category_item_node_path AFTER INSERT OR UPDATE OF category_id, parent_id
   ON market.item_category FOR EACH ROW
   EXECUTE PROCEDURE trigger_update_item_category_node_path();

INSERT INTO market.item_category(category_id, parent_id, label) VALUES 
('001', NULL, 'All'),
('002', '001', 'Fruit'),
    ('003', '002', 'Citrus'),
    ('004', '002', 'Berries'),
    ('005', '002', 'Tropical & Specialty'),
('006', '001', 'Vegetables'),
    ('007', '006', 'Greens'),
    ('008', '006', 'Roots'),
    ('009', '006', 'Mushrooms'),
    ('010', '006', 'Herbs'),
('011', '001', 'Dairy & Eggs'),
    ('012', '011', 'Eggs'),
('013', '001', 'Artisanal'),
    ('014', '013', 'Honey'),
    ('015', '013', 'Pickles & Ferments'),
('016', '001', 'Other');

CREATE TYPE UOM_Type AS ENUM ('each', 'bunch', 'ounce', 'pound', 'gram', 'kilogram', 'bushel', 'peck', 'fluid ounce', 'gallon', 'liter');

CREATE TABLE market.item(
    item_id         SERIAL PRIMARY KEY,
    item_uuid       uuid DEFAULT gen_random_uuid(),
    product_code    varchar(128),
    supplier_id     integer REFERENCES entity.organization(org_id) ON DELETE CASCADE,
    supplier_uuid   uuid NOT NULL,
    uom             UOM_Type NOT NULL DEFAULT 'pound',
    status          varchar(64) NOT NULL,                   -- status of the item: active | deleted
    title           varchar(128) NOT NULL,
    description     text,
    category        ltree NOT NULL,
    thumbnail       bytea,
    created         timestamptz NOT NULL DEFAULT now(),
    modified        timestamptz NOT NULL DEFAULT now(),
    extended        jsonb
);

CREATE INDEX item_status_index ON market.item (status);
CREATE INDEX item_title_index ON market.item USING GIN (to_tsvector('english', title));
CREATE INDEX item_description_index ON market.item USING GIN (to_tsvector('english', description));
CREATE INDEX item_category_index ON market.item (category);
CREATE INDEX item_created_index ON market.item (created);

-- Create a "Default Item"
INSERT INTO market.item (item_id, item_uuid, supplier_id, supplier_uuid, status, title, category) VALUES (nextval(pg_get_serial_sequence('market.item', 'item_id')), 'ffffffff-ffff-ffff-ffff-ffffffffffff', 1, 'ffffffff-ffff-ffff-ffff-ffffffffffff', 'deleted', 'Default Item', '001');

CREATE TABLE market.item_image(
    image_id        SERIAL PRIMARY KEY,
    image_uuid      uuid DEFAULT gen_random_uuid(),
    item_id         integer REFERENCES market.item ON DELETE CASCADE,
    item_uuid       uuid NOT NULL,
    name            varchar(128),
    description     varchar(128),
    format          varchar(128),
    width           integer,
    height          integer,
    image           bytea
);

CREATE INDEX item_image_uuid_index ON market.item_image(image_uuid);
CREATE INDEX item_image_item_uuid_index ON market.item_image(item_uuid);

CREATE TABLE market.item_pricing(
    pricing_id      SERIAL PRIMARY KEY,
    pricing_uuid    uuid DEFAULT gen_random_uuid(),
    item_id         integer REFERENCES market.item ON DELETE CASCADE,
    item_uuid       uuid NOT NULL,
    quantity_min    double precision NOT NULL,
    quantity_max    double precision NOT NULL,
    price           double precision NOT NULL
);

CREATE INDEX item_pricing_item_uuid_index ON market.item_pricing(item_uuid);

CREATE TABLE market.item_attribute(
    item_id         integer REFERENCES market.item ON DELETE CASCADE,
    item_uuid       uuid NOT NULL,
    attribute       varchar(128) NOT NULL,
    UNIQUE (item_id, attribute)
);

CREATE INDEX item_attribute_item_id_index ON market.item_attribute(item_id);
CREATE INDEX item_attribute_index ON market.item_attribute(attribute);
CREATE INDEX item_attribute_item_uuid_index ON market.item_attribute(item_uuid);

COMMIT;