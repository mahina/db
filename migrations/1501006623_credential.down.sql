BEGIN;

DROP TABLE entity.credential;

DROP FUNCTION entity.insert_credential(integer, credential_type, text, text);

DROP FUNCTION entity.update_credential(integer, credential_type, text, text);

DROP FUNCTION entity.verify_credential(integer, credential_type, text, text);

DROP TYPE credential_type;

COMMIT;