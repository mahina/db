BEGIN;

DROP TABLE market.item_image;
DROP TABLE market.item_pricing;
DROP TABLE market.item_attribute;
DROP TABLE market.item;

DROP TYPE UOM_Type;

DROP TABLE market.item_category;

DROP FUNCTION get_calculated_item_category_node_path(param_item_category_id text);
DROP FUNCTION trigger_update_item_category_node_path();

COMMIT;