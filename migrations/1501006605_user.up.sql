BEGIN;

SET client_min_messages = 'warning';

CREATE TABLE entity.user (
    user_id         serial PRIMARY KEY,
    user_uuid       uuid DEFAULT gen_random_uuid(),
    status          varchar(16),
    username        varchar(254) NOT NULL UNIQUE,
    first_name      varchar(64) NOT NULL,
    last_name       varchar(64) NOT NULL,
    middle_name     varchar(64),
    title           varchar(24),
    suffix          varchar(24),
    job_title       varchar(64),
    birth_date      date,
    gender          char(2),
    timezone        varchar(8),
    locale          varchar(8),
    email           varchar(254),
    work_phone      varchar(24),
    home_phone      varchar(24),
    mobile_phone    varchar(24),
    fax_phone       varchar(24),
    image_icon      bytea,
    created         timestamptz NOT NULL DEFAULT now(),
    extended        jsonb
);

CREATE INDEX user_uuid_index ON entity.user (user_uuid);
CREATE INDEX user_username_index ON entity.user (username);
CREATE INDEX user_email_index ON entity.user (email);

COMMIT;
