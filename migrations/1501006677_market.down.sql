BEGIN;

SET client_min_messages = 'warning';

DROP TABLE entity.organization_market;

DROP TABLE entity.market;

COMMIT;