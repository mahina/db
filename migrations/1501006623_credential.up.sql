BEGIN;

SET client_min_messages = 'warning';

-- Support for other credential types in the future (retina data, etc)
CREATE TYPE credential_type AS ENUM ('password');

CREATE TABLE entity.credential (
    user_id integer REFERENCES entity.user ON DELETE CASCADE,
    type            credential_type NOT NULL,
    challenge       text NOT NULL,      -- first part of challenge (e.g. login, oauth grant)
    response        text NOT NULL,      -- second part of challenge (e.g. password, fingerprint data, oauth token). This must be crypt()-ed
    UNIQUE (user_id, type)
);

CREATE OR REPLACE FUNCTION entity.insert_credential(
    user_id integer,
    cred_type credential_type,
    challenge text,
    response text
) RETURNS VOID LANGUAGE SQL SECURITY DEFINER AS $$
    INSERT INTO entity.credential VALUES ($1, $2, $3, crypt($4, gen_salt('md5')));
$$;

CREATE OR REPLACE FUNCTION entity.update_credential(
      user_id integer,
      cred_type credential_type,
      old_response text,
      new_response text 
) RETURNS BOOLEAN LANGUAGE plpgsql SECURITY DEFINER AS $$
  BEGIN
      UPDATE entity.credential
         SET response = crypt($4, gen_salt('md5'))
       WHERE entity.credential.user_id = $1
         AND type = $2
         AND response = crypt($3, response);
      RETURN FOUND;
  END;
$$;

CREATE OR REPLACE FUNCTION entity.verify_credential(
      user_id integer,
      cred_type credential_type,
      challenge text,
      response text
) RETURNS integer LANGUAGE plpgsql SECURITY DEFINER AS $$
  DECLARE
  userid int;
  BEGIN
      SELECT entity.credential.user_id INTO STRICT userid FROM entity.credential 
        WHERE entity.credential.user_id = $1 
        AND type = $2 
        AND entity.credential.challenge = $3  
        AND entity.credential.response = crypt($4, entity.credential.response);
      RETURN userid;
  END;
$$;

COMMIT;