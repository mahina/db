BEGIN;

DROP TABLE market.order_exception;
DROP TABLE market.order_item;
DROP TABLE market.standing_order_item;
DROP TABLE market.order;

COMMIT;