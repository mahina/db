BEGIN;

CREATE TABLE market.order(
    order_id            SERIAL PRIMARY KEY,
    order_uuid          uuid DEFAULT gen_random_uuid(),
    buyer_id            integer REFERENCES entity.organization(org_id),
    buyer_uuid          uuid NOT NULL,
    buyer_user_id       integer REFERENCES entity.user(user_id),
    buyer_user_uuid     uuid NOT NULL,
    type                varchar(16) NOT NULL DEFAULT 'spot' CHECK (type IN ('spot','standing')),
    delivery_date       date NOT NULL,  
    last_delivery_date  date,                                                   -- for standing orders                                       
    notes               text,
    created             timestamptz NOT NULL DEFAULT now(),
    modified            timestamptz NOT NULL DEFAULT now(),
    extended            jsonb
);

CREATE INDEX order_buyer_uuid_index ON market.order(buyer_uuid);
CREATE INDEX order_buyer_user_uuid_index ON market.order(buyer_user_uuid);

CREATE TABLE market.order_item(
    order_item_id       SERIAL PRIMARY KEY,
    order_item_uuid     uuid DEFAULT gen_random_uuid(),
    order_id            integer REFERENCES market.order ON DELETE CASCADE,
    order_uuid          uuid NOT NULL,
    supplier_id         integer REFERENCES entity.organization(org_id),
    supplier_uuid       uuid NOT NULL,
    item_id             integer REFERENCES market.item ON DELETE CASCADE,
    item_uuid           uuid NOT NULL,
    offer_id            integer REFERENCES market.offer ON DELETE SET NULL,
    offer_uuid          uuid,
    line_item           integer,
    quantity            double precision,
    price               double precision,
    notes               text,
    created             timestamptz NOT NULL DEFAULT now(),
    extended            jsonb
);

CREATE INDEX order_item_order_uuid_index on market.order_item(order_uuid);
CREATE INDEX order_item_offer_uuid_index on market.order_item(offer_uuid);

CREATE TABLE market.standing_order_item(
    order_item_id       SERIAL PRIMARY KEY,
    order_item_uuid     uuid DEFAULT gen_random_uuid(),
    order_id            integer REFERENCES market.order ON DELETE CASCADE,
    order_uuid          uuid NOT NULL,
    supplier_id         integer REFERENCES entity.organization(org_id),
    supplier_uuid       uuid NOT NULL,
    item_id             integer REFERENCES market.item ON DELETE CASCADE,
    item_uuid           uuid NOT NULL,
    offer_id            integer REFERENCES market.offer ON DELETE SET NULL,
    offer_uuid          uuid,
    line_item           integer,
    quantity_by_dow     double precision[7], -- 0 = Monday
    frequency           varchar(24) NOT NULL CHECK (frequency IN ('daily','weekly','biweekly','monthly')),
    price               double precision,
    notes               text,
    created             timestamptz NOT NULL DEFAULT now(),
    extended            jsonb    
);

CREATE INDEX standing_order_item_order_uuid_index on market.standing_order_item(order_uuid);
CREATE INDEX standing_order_item_offer_uuid_index on market.standing_order_item(offer_uuid);

-- For standing orders, this table records delivery blackout dates
CREATE TABLE market.order_exception(
    order_id            integer REFERENCES market.order_item ON DELETE CASCADE,
    exception_date      date NOT NULL,
    notes               text
);

CREATE INDEX order_exception_exception_date_index ON market.order_exception(exception_date);

COMMIT;