BEGIN;

CREATE TABLE market.message(
    message_id      SERIAL PRIMARY KEY,
    message_uuid    uuid DEFAULT gen_random_uuid(),
    sender          uuid NOT NULL,
    recipient       uuid NOT NULL,
    artifact_type   varchar(64),
    artifact_uuid   uuid,
    subject         varchar(128),
    content         text,
    created         timestamptz NOT NULL DEFAULT now(),
    delivered       timestamptz,
    extended        jsonb
);

CREATE INDEX message_uuid_index ON market.message (message_uuid);
CREATE INDEX message_sender_index ON market.message (sender);
CREATE INDEX message_recipient_index ON market.message (recipient);
CREATE INDEX message_artifact_type ON market.message (artifact_type);
CREATE INDEX message_artifact_index ON market.message (artifact_uuid);

COMMIT;