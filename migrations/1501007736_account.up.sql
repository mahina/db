BEGIN;

SET client_min_messages = 'warning';

CREATE TABLE entity.account (
    account_id      serial PRIMARY KEY,
    buyer_id        integer REFERENCES entity.organization (org_id) ON DELETE CASCADE,
    supplier_id     integer REFERENCES entity.organization (org_id) ON DELETE CASCADE,
    name            varchar(24) DEFAULT 'default',
    terms           varchar(24),                            -- e.g. NET15
    created         timestamptz NOT NULL DEFAULT now(),
    start_date      timestamptz,
    end_date        timestamptz,
    extended        jsonb,
    UNIQUE (buyer_id, supplier_id, name)
);

COMMIT;
