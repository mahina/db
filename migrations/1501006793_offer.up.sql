BEGIN;

CREATE TABLE market.offer(
    offer_id        SERIAL PRIMARY KEY,
    offer_uuid      uuid DEFAULT gen_random_uuid(),
    supplier_id     integer REFERENCES entity.organization(org_id) ON DELETE CASCADE,
    supplier_uuid   uuid NOT NULL,
    item_id         integer REFERENCES market.item ON DELETE CASCADE,
    item_uuid       uuid NOT NULL,
    status          varchar(64) NOT NULL,           -- ACTIVE | INACTIVE | DELETED: status of the offer
    type            varchar(128) NOT NULL CHECK (type IN ('willsupply','couldsupply','willbuy','couldbuy')),
    first_date      date NOT NULL,
    last_date       date NOT NULL,
    quantity        double precision,
    delivery_days   integer NOT NULL,               -- bitmask, position 1 = monday, 2 = tuesday... 7 = sunday
    created         timestamptz NOT NULL DEFAULT now(),
    modified        timestamptz NOT NULL DEFAULT now(),
    notes           text,
    extended        jsonb
);

CREATE INDEX offer_uuid_index ON market.offer(offer_uuid);
CREATE INDEX offer_supplier_uuid_index ON market.offer(supplier_uuid);
CREATE INDEX offer_status_index ON market.offer(status);
CREATE INDEX offer_type_index ON market.offer(type);
CREATE INDEX offer_first_date_index ON market.offer(first_date);
CREATE INDEX offer_last_date_index ON market.offer(last_date);
CREATE INDEX offer_modified_index ON market.offer(modified);
CREATE INDEX offer_item_id_index ON market.offer(item_id);

CREATE TABLE market.offer_pricing(
    pricing_id      SERIAL PRIMARY KEY,
    pricing_uuid    uuid DEFAULT gen_random_uuid(),
    offer_id        integer REFERENCES market.offer ON DELETE CASCADE,
    offer_uuid      uuid NOT NULL,
    quantity_min    double precision NOT NULL,
    quantity_max    double precision NOT NULL,
    price           double precision NOT NULL
);

CREATE INDEX offer_pricing_offer_uuid_index ON market.offer_pricing(offer_uuid);

COMMIT;