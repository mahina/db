BEGIN;

DROP SCHEMA entity;

DROP FUNCTION count_estimate(query text);

COMMIT;
