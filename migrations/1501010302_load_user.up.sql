BEGIN;

-- insert demo user 
DO $$DECLARE
    userId integer;
    orgId integer;
    marketId integer;
BEGIN
    INSERT INTO entity.user (user_uuid, status, username, first_name, last_name, job_title, email, mobile_phone) VALUES 
        ('ffffffff-ffff-ffff-ffff-ffffffffffff', 'active', 'joe.mayo@gomahina.com', 'Joe', 'Mayo', 'Grower', 'joe.mayo@gomahina.com', '212-555-1212') RETURNING
        user_id INTO userId;
    INSERT INTO entity.user_role VALUES (userId, 'supplier');
    PERFORM entity.insert_credential(userId, 'password', 'joe.mayo@gomahina.com', 'massagechair');
    INSERT INTO entity.user_organization VALUES (userId, 1);
    INSERT INTO entity.market (status, name, schema_name) 
        VALUES ('active', 'default', 'market') RETURNING market_id INTO marketId;

    INSERT INTO entity.organization_market VALUES (1, marketId);
END$$;

COMMIT;