BEGIN;

CREATE TABLE market.inventory(
    item_id     integer REFERENCES market.item ON DELETE CASCADE,
    item_uuid   uuid NOT NULL,
    quantity    double precision NOT NULL DEFAULT 0.0,
    record_date   timestamptz NOT NULL DEFAULT now(),
    notes       text,
    extended    jsonb
);

CREATE INDEX inventory_item_uuid_index ON market.inventory(item_uuid);
CREATE INDEX inventory_record_date_index ON market.inventory(record_date);

COMMIT;
