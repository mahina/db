BEGIN;

SET client_min_messages = 'warning';

CREATE TABLE entity.market (
    market_id       serial PRIMARY KEY,
    market_uuid     uuid DEFAULT gen_random_uuid(),
    status          varchar(16),
    name            varchar(64) NOT NULL UNIQUE,
    schema_name     varchar(64),
    created         timestamptz NOT NULL DEFAULT now(),
    extended        jsonb
);

CREATE INDEX market_name_index ON entity.market (name);

CREATE TABLE entity.organization_market (
    org_id integer REFERENCES entity.organization ON DELETE CASCADE,
    market_id integer REFERENCES entity.market ON DELETE CASCADE
);

COMMIT;