BEGIN;

DROP TABLE entity.user_organization;

DROP TABLE entity.organization;

DROP TYPE organization_type;

COMMIT;
