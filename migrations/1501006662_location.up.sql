BEGIN;

SET client_min_messages = 'warning';

CREATE TABLE entity.location (
    org_id          integer REFERENCES entity.organization ON DELETE CASCADE,
    name            varchar(24) DEFAULT 'default',
    address_line1   varchar(128) NOT NULL,
    address_line2   varchar(128),
    address_line3   varchar(128),
    address_line4   varchar(128),
    locality        varchar(64) NOT NULL,       -- aka city
    region          varchar(64) NOT NULL,       -- aka state
    postcode        varchar(16) NOT NULL,       -- aka zipcode
    country         varchar(32) NOT NULL,
    phone           varchar(24),
    fax_phone       varchar(24),
    created         timestamptz NOT NULL DEFAULT now(),
    extended        jsonb,                       -- plan to store GIS data here (not in columns)
    UNIQUE (org_id, name)
);
 
COMMIT;
