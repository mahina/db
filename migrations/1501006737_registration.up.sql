BEGIN;

SET client_min_messages = 'warning';

CREATE TABLE entity.registration (
    reg_uuid        uuid PRIMARY KEY DEFAULT gen_random_uuid(),
    username        varchar(254) UNIQUE NOT NULL,
    status          varchar(32) DEFAULT 'received',
    created         timestamptz NOT NULL DEFAULT now(),
    ip_address      varchar(16),
    extended        jsonb
);

CREATE INDEX registration_uuid_index ON entity.registration (reg_uuid);
CREATE INDEX registration_username_index ON entity.registration (username);
CREATE INDEX registration_status_index ON entity.registration (status);

CREATE TABLE entity.registration_action (
    reg_uuid        uuid REFERENCES entity.registration (reg_uuid) ON DELETE CASCADE,
    username        varchar(254) NOT NULL,
    action          varchar(32) NOT NULL,
    created         timestamptz NOT NULL DEFAULT now(),
    log             text
);

CREATE INDEX registration_action_uuid_index ON entity.registration_action (reg_uuid);
CREATE INDEX registration_action_username_index ON entity.registration_action (username);
CREATE INDEX registration_action_created_index ON entity.registration_action (created);

COMMIT;
