BEGIN;

SET client_min_messages = 'warning';

CREATE TABLE entity.role (
    role_id     varchar(16) PRIMARY KEY,
    description varchar(64) NOT NULL
);

INSERT INTO entity.role VALUES ('supplier', 'Supplies goods or services');
INSERT INTO entity.role VALUES ('buyer', 'Buys goods or services');
INSERT INTO entity.role VALUES ('admin', 'Administers entities and the marketplace');
INSERT INTO entity.role VALUES ('researcher', 'Gets reports from the marketplace');
INSERT INTO entity.role VALUES ('public', 'Accesses the marketplace without credentials');

CREATE TABLE entity.user_role (
    user_id integer REFERENCES entity.user ON DELETE CASCADE,
    role_id varchar(16) REFERENCES entity.role ON DELETE CASCADE,
    PRIMARY KEY (user_id, role_id)
);

COMMIT;