BEGIN;

SET client_min_messages = 'warning';

CREATE TYPE organization_type AS ENUM ('unknown', 'charity', 'nonprofit', 'govt', 'ngo', 'sole', 'llc', 'corp');

CREATE TABLE entity.organization (
    org_id          serial PRIMARY KEY,
    org_uuid        uuid DEFAULT gen_random_uuid(),
    status          varchar(16),
    legal_name      varchar(128) NOT NULL UNIQUE,
    dba             varchar(128),
    short_name      varchar(24),
    org_type        organization_type DEFAULT 'unknown',        
    duns            varchar(9),
    employee_count  integer,
    image_logo      bytea,
    created         timestamptz NOT NULL DEFAULT now(),
    extended        jsonb
);

CREATE INDEX organization_legalname_index ON entity.organization (legal_name);
CREATE INDEX organization_org_uuid_index ON entity.organization (org_uuid);
CREATE INDEX organization_legalname_gin_index ON entity.organization USING GIN (to_tsvector('english', legal_name));

-- Create the "Default Organization"
INSERT INTO entity.organization (org_id, org_uuid, legal_name) VALUES (nextval(pg_get_serial_sequence('entity.organization', 'org_id')), 'ffffffff-ffff-ffff-ffff-ffffffffffff', 'Default Marketplace Org');


CREATE TABLE entity.user_organization (
    user_id integer REFERENCES entity.user ON DELETE CASCADE,
    org_id integer REFERENCES entity.organization ON DELETE CASCADE
);

COMMIT;