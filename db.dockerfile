FROM postgres:alpine
ENV DB_NAME market
ENV DB_USER mahina
ENV DB_PASS m@hInA

# Database Setup
# ------------------------------------------------------------------------------

# ADD the db certificates
ADD certs/server.key /var/lib/postgresql/server.key
ADD certs/server.crt /var/lib/postgresql/server.crt
ADD certs/root.crt /var/lib/postgresql/root.crt
RUN chown postgres /var/lib/postgresql/server.key
RUN chown postgres /var/lib/postgresql/server.crt
RUN chown postgres /var/lib/postgresql/root.crt
RUN chmod 0600 /var/lib/postgresql/server.key
RUN chmod 0600 /var/lib/postgresql/server.crt
RUN chmod 0600 /var/lib/postgresql/root.crt

# set the ssl = on flag
ADD docker-artifacts/update_postgresql_conf.sh /docker-entrypoint-initdb.d/0update_postgresql_conf.sh
# update the postgres access file
ADD docker-artifacts/update_pg_hba.sh /docker-entrypoint-initdb.d/1update_pg_hba.sh
# install the cert and key into the data directory
ADD docker-artifacts/install_certs.sh /docker-entrypoint-initdb.d/2install_certs.sh
# setup the database and user
ADD docker-artifacts/setup_db.sh /docker-entrypoint-initdb.d/3setup_db.sh
# install the migrate (postgres) utility
ADD migrate-linux /usr/local/bin/migrate
RUN chown postgres /usr/local/bin/migrate
RUN chmod +x /usr/local/bin/migrate
# add the migration files to the container
ADD migrations /tmp/migrations
# add the migration script to the entrypoint
ADD docker-artifacts/run_migrations.sh /docker-entrypoint-initdb.d/4run_migrations.sh
# add the unit tests and invoker
ADD test /tmp/db_tests
ADD docker-artifacts/run_tests.sh /tmp/run_tests.sh
RUN chmod +x /tmp/run_tests.sh



