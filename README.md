# Mahina DB

This is the database module for the Mahina Project. The Mahina Food Marketplace Platform is an open source project looking to revolutionize how suppliers and buyers conduct business in local food markets.

Based on Postgres 9.6+, this module uses `github.com/mattes/migrate` to build the db from `./migrations`. We use Docker to easily manage running the db locally and in testing. The db container is based from Alpine, so it only takes about about 50MB.

## Running the DB

The simpliest way to run the DB is to use the Docker container that lives at https://gitlab.com/mahina/db/container_registry

```
$ export POSTGRES_SU_PASSWORD=xxxxxxxx
$ export DATABASE_NAME=xxxxxxxxx
$ export DATABASE_USER=xxxxxxxxx
$ export DATABASE_PASSWORD=xxxxxxxxx
$ docker run -d -e POSTGRES_PASSWORD=$POSTGRES_SU_PASSWORD -e DB_USER=$DATABASE_USER \
  -e DB_NAME=$DATABASE_NAME -e DB_PASS=$DATABASE_PASSWORD -p 5432:5432 --name mahina-db \
  registry.gitlab.com/mahina/db
```

## Building the DB

Simply committing to this repo[^1] will launch the gitlab-based CI which will build the mahina/db container which you can then run as described above.

To build and run locally, have a look at the `.gitlab-ci.yml` file—in which you can grok the steps needed to run the migrations and tests.

## Tests

Find tests in the `test` folder. These tests are part of the `docker run` process.

[^1]: Presently there is no branch-specific distinction in container names