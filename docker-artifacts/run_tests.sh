echo "Waiting to run tests... db: ${DB_NAME} user: ${DB_USER}"
export PGPASSWORD=$DB_PASS
until psql -U ${DB_USER} ${DB_NAME} -h localhost -v "ON_ERROR_STOP=1" -c "select * from entity.user;"; do echo "waiting"; sleep 1; done
sleep 5
echo "Starting tests"
psql -U $DB_USER $DB_NAME -f /tmp/db_tests/entity-test.sql
psql -U $DB_USER $DB_NAME -f /tmp/db_tests/registration-test.sql
