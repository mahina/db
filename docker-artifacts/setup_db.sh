#!/bin/bash
created=`psql -U postgres -c 'SELECT datname FROM pg_database' | grep $DB_NAME | wc -l`
if [ "$created" -eq "0" ]; then
    echo "Creating database"
    psql -U postgres -c "CREATE ROLE $DB_USER WITH LOGIN ENCRYPTED PASSWORD '${DB_PASS}';"
    psql -U postgres -c "CREATE DATABASE $DB_NAME WITH OWNER $DB_USER TEMPLATE template0 ENCODING 'UTF8';"
    psql -U postgres -c "GRANT ALL PRIVILEGES ON DATABASE $DB_NAME TO $DB_USER;"
    psql -U postgres $DB_NAME -c "CREATE EXTENSION pgcrypto;"
    psql -U postgres $DB_NAME -c "CREATE EXTENSION fuzzystrmatch;"
    psql -U postgres $DB_NAME -c "CREATE EXTENSION ltree;"
else
    echo "Database found, bypassing database creation"
fi

