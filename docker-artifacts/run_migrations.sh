#!/bin/bash
until pg_isready
do
    echo "."
    sleep 1
done
sleep 2
echo "Starting migration"
/usr/local/bin/migrate -verbose -database postgres://$DB_USER:$DB_PASS@localhost:5432/$DB_NAME?sslmode=disable -path /tmp/migrations up
