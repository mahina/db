-- Verify platform:entityschema-test on pg

/*
  Unit tests for the entity schema

  - user: insert √
  - org: insert √
  - org: insert with illegal null legal name √
  - org: insert with illegal org type √
  - credential: insert √
  - credential: insert with illegal type √
  - credential: insert with non-existent user_id √
  - credential: insert duplicate user_id, type √
  - credential: insert encrypted credential, decrypt √
  - credential: update credential, decrypt √
  - credential: user deleted, ensure credential is removed √
  - user-role: insert √
  - user-role: insert with non-existent user_id √
  - user-role: insert with non-existent role_type √
  - user-role: delete role, ensure mapping in removed √
  - user-role: delete user, ensure mapping is removed √
  - user-organization: insert √
  - user-organization: insert with non-existent user_id √
  - user-organization: insert with non-existent org_id √
  - user-organization: delete user, ensure mapping is removed √
  - user-organization: delete org, ensure mapping is removed √
  - location: insert √
  - location: insert with illegal null fields √
  - location: insert with non-existent org_id √
  - location: delete org, ensure cascade √
  - account: insert √
  - account: insert with non-existent org_id √
  - account: delete org, ensure cascade √
  - market: insert new market √
  - organization-market: insert bridge record √
*/

SET client_min_messages = 'notice';

BEGIN;

-- User Insert Tests
-- -----------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION TestInsertUser() RETURNS integer AS $$
DECLARE
    userId integer;
    userUUID uuid;
BEGIN
    INSERT INTO entity.user (username, first_name, last_name, email) 
        VALUES ('foo.bar+foo-bar@gmail.com', 'Mateo', 'McNeely', 'foo.bar+foo-bar@gmail.com') RETURNING user_id, user_uuid INTO userId, userUUID;

    IF userUUID IS NULL
    THEN
        RAISE EXCEPTION 'User UUID unexpectedly empty';
    END IF;
    RETURN userId;
END;
$$ LANGUAGE plpgsql;

-- Organization Insert Tests
-- -----------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION TestInsertOrg() RETURNS integer AS $$
DECLARE
    orgId integer;
BEGIN
    INSERT INTO entity.organization (legal_name) VALUES ('Acme Lettuce') RETURNING org_id INTO orgId;
    -- Ensure an empty legal name cannot be inserted
    BEGIN
        INSERT INTO entity.organization (legal_name) VALUES (NULL);
        EXCEPTION
            WHEN not_null_violation THEN    -- expected
            WHEN OTHERS THEN
                RAISE EXCEPTION 'Unexpected error raised: %, %', SQLERRM, SQLSTATE;
    END;
    -- Ensure a duplicate org legal name cannot be inserted
    BEGIN
        INSERT INTO entity.organization (legal_name) VALUES ('Acme Lettuce');
        EXCEPTION
            WHEN unique_violation THEN    -- expected
            WHEN OTHERS THEN
                RAISE EXCEPTION 'Unexpected error raised: %, %', SQLERRM, SQLSTATE;
    END;
    -- Ensure an illegal org type cannot be inserted 
    BEGIN
        INSERT INTO entity.organization (legal_name, org_type) VALUES ('Acme Beans', 'IllegalType');
        EXCEPTION
            WHEN invalid_text_representation THEN -- expected 
            WHEN OTHERS THEN
                RAISE EXCEPTION 'Unexpected error raised: %, %', SQLERRM, SQLSTATE;
    END;

    RETURN orgId;
END;
$$ LANGUAGE plpgsql;

-- Credential Tests
-- -----------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION TestCredentials(userId integer) RETURNS void AS $$
DECLARE
    password text := 'mYsecretPassw0rd';
BEGIN
    PERFORM entity.insert_credential(userId, 'password', 'login', password);
    -- Ensure an illegal credential type cannot be inserted 
    BEGIN
        PERFORM entity.insert_credential(userId, 'some_unknown_credential_type', 'login', password);
        EXCEPTION
            WHEN invalid_text_representation THEN -- expected 
            WHEN OTHERS THEN
                RAISE EXCEPTION 'Unexpected error raised: %, %', SQLERRM, SQLSTATE;
    END;
    -- Ensure a illegal userId cannot be inserted 
    BEGIN
        PERFORM entity.insert_credential(userId+1000000, 'password', 'login', password);
        EXCEPTION
            WHEN foreign_key_violation THEN -- expected 
            WHEN OTHERS THEN
                RAISE EXCEPTION 'Unexpected error raised: %, %', SQLERRM, SQLSTATE;
    END;
    -- Ensure duplicate user_id and credendial type cannot be added
    BEGIN
        PERFORM entity.insert_credential(userId, 'password', 'login', password);
        EXCEPTION
            WHEN unique_violation THEN -- expected 
            WHEN OTHERS THEN
                RAISE EXCEPTION 'Unexpected error raised: %, %', SQLERRM, SQLSTATE;
    END;
    -- Decrypt password 
    DECLARE
        foundUserId integer;
    BEGIN
        SELECT entity.verify_credential(userId, 'password', 'login', password) INTO foundUserId;
        IF foundUserId <> userId 
        THEN
            RAISE EXCEPTION 'Password decryption failed';
        END IF;
    END;
    -- Decrypt password incorrectly
    DECLARE
        foundUserId integer;
    BEGIN
        SELECT entity.verify_credential(userId, 'password', 'login', 'this is not my password') INTO foundUserId;
        EXCEPTION
            WHEN no_data_found THEN -- expected 
            WHEN OTHERS THEN
                RAISE EXCEPTION 'Unexpected error raised: %, %', SQLERRM, SQLSTATE;
    END;
    -- Try to update a password incorrectly
    DECLARE
        found boolean;
    BEGIN
        SELECT entity.update_credential(userId, 'password', 'this is clearly not my password', password) INTO found;
        IF found = true
        THEN
            RAISE EXCEPTION 'Error: updating password with incorrect value not caught';
        END IF;
    END;
    -- Update password
    password = 'mynewpassW0rd';
    PERFORM entity.update_credential(userId, 'password', 'mYsecretPassw0rd', password);
    -- Decrypt password 
    DECLARE
        foundUserId integer;
    BEGIN
        SELECT entity.verify_credential(userId, 'password', 'login', password) INTO foundUserId;
        IF foundUserId <> userId 
        THEN
            RAISE EXCEPTION 'Password decryption failed following update';
        END IF;
    END;
    -- Remove user, associated credential should be CASCADE-d delete
    DECLARE
        count integer;
    BEGIN
        DELETE FROM entity.user WHERE user_id = userId;
        SELECT COUNT(*) FROM entity.credential WHERE user_id = userId INTO count;
        IF count > 0
        THEN
            RAISE EXCEPTION 'Cascade delete of credential failed';
        END IF;
    END;

END;
$$ LANGUAGE plpgsql;

-- User-Role Tests
-- -----------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION TestUserRole() RETURNS void AS $$
DECLARE
    userId integer;
    count integer;
BEGIN
    SELECT TestInsertUser() INTO userId;
    INSERT INTO entity.user_role VALUES (userId, 'buyer');

    -- Test violation of foreign key constraints
    BEGIN
        INSERT INTO entity.user_role VALUES (userId + 2000000, 'supplier');
        EXCEPTION
            WHEN foreign_key_violation THEN -- expected 
            WHEN OTHERS THEN
                RAISE EXCEPTION 'Unexpected error raised: %, %', SQLERRM, SQLSTATE;
    END;
    BEGIN
        INSERT INTO entity.user_role VALUES (userId, 'invalid role');
        EXCEPTION
            WHEN foreign_key_violation THEN -- expected 
            WHEN OTHERS THEN
                RAISE EXCEPTION 'Unexpected error raised: %, %', SQLERRM, SQLSTATE;
    END;
    -- Test cascade delete constraints
    DELETE FROM entity.role WHERE role_id = 'buyer';
    SELECT COUNT(*) FROM entity.user_role WHERE role_id = 'buyer' AND user_id = userId INTO count;
    IF count > 0
    THEN
        RAISE EXCEPTION 'Error in Cascade delete of role from user_role';
    END IF;

    INSERT INTO entity.user_role VALUES (userId, 'supplier');
    DELETE FROM entity.user WHERE user_id = userId;
    SELECT COUNT(*) FROM entity.user_role WHERE role_id = 'supplier' AND user_id = userId INTO count;
    IF count > 0
    THEN
        RAISE EXCEPTION 'Error in Cascade delete of user from user_role';
    END IF;

END;
$$ LANGUAGE plpgsql;

-- User-Organization Tests
-- -----------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION TestUserOrganizationBridge() RETURNS void AS $$
DECLARE
    userId integer;
    orgId integer;
    count integer;
BEGIN
    SELECT TestInsertUser() INTO userId;
    INSERT INTO entity.organization (legal_name) VALUES ('Acme Sprouts') RETURNING org_id INTO orgId;
    INSERT INTO entity.user_organization VALUES (userId, orgId);

    -- Test violation of foreign key constraints
    BEGIN
        INSERT INTO entity.user_organization VALUES (userId + 2000001, orgId);
        EXCEPTION
            WHEN foreign_key_violation THEN -- expected 
            WHEN OTHERS THEN
                RAISE EXCEPTION 'Unexpected error raised: %, %', SQLERRM, SQLSTATE;
    END;
    BEGIN
        INSERT INTO entity.user_organization VALUES (userId, orgId + 2000001);
        EXCEPTION
            WHEN foreign_key_violation THEN -- expected 
            WHEN OTHERS THEN
                RAISE EXCEPTION 'Unexpected error raised: %, %', SQLERRM, SQLSTATE;
    END;
    -- Test cascade delete constraints
    DELETE FROM entity.organization WHERE org_id = orgId;
    SELECT COUNT(*) FROM entity.user_organization WHERE org_id = orgId AND user_id = userId INTO count;
    IF count > 0
    THEN
        RAISE EXCEPTION 'Error in Cascade delete of org from user_organization';
    END IF;
    INSERT INTO entity.organization (legal_name) VALUES ('Acme Sprouts') RETURNING org_id INTO orgId;
    INSERT INTO entity.user_organization VALUES (userId, orgId);
    DELETE FROM entity.user WHERE user_id = userId;
    SELECT COUNT(*) FROM entity.user_organization WHERE org_id = orgId AND user_id = userId INTO count;
    IF count > 0
    THEN
        RAISE EXCEPTION 'Error in Cascade delete of user from user_organization';
    END IF;
    
END;
$$ LANGUAGE plpgsql;

-- Location Tests
-- -----------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION TestLocation() RETURNS void AS $$
DECLARE
    orgId integer;
    count integer;
BEGIN
    INSERT INTO entity.organization (legal_name) VALUES ('Acme Carrots') RETURNING org_id INTO orgId;
    INSERT INTO entity.location (org_id, name, address_line1, locality, region, postcode, country)
        VALUES (orgId, 'hq', '121 Main St', 'Epping', 'NH', '03042-2707', 'USA');
    INSERT INTO entity.location (org_id, name, address_line1, locality, region, postcode, country)
        VALUES (orgId, 'warehouse', '666 Elm St', 'Epping', 'NH', '03042-2707', 'USA');
    SELECT COUNT(*) FROM entity.location WHERE org_id = orgId INTO count;
    IF count <> 2
    THEN
        RAISE EXCEPTION 'Error in insert of multiple locations';
    END IF;
    
    -- Test missing NOT NULL fields
    BEGIN    
        INSERT INTO entity.location (org_id, name, address_line1, locality, region, postcode)
            VALUES (orgId, 'warehouse', '666 Elm St', 'Epping', 'NH', '03042-2707');
        EXCEPTION
            WHEN not_null_violation THEN    -- expected
            WHEN OTHERS THEN
                RAISE EXCEPTION 'Unexpected error raised: %, %', SQLERRM, SQLSTATE;
    END;
    -- Test foreign key (org_id) constraint
    BEGIN    
        INSERT INTO entity.location (org_id, name, address_line1, locality, region, postcode, country)
            VALUES (orgId + 2000001, 'warehouse', '666 Elm St', 'Epping', 'NH', '03042-2707', 'USA');
        EXCEPTION
            WHEN foreign_key_violation THEN -- expected 
            WHEN OTHERS THEN
                RAISE EXCEPTION 'Unexpected error raised: %, %', SQLERRM, SQLSTATE;
    END;
    -- Test cascade delete constraints
    DELETE FROM entity.organization WHERE org_id = orgId;
    SELECT COUNT(*) FROM entity.location WHERE org_id = orgId INTO count;
    IF count <> 0
    THEN
        RAISE EXCEPTION 'Error in Cascade delete from locations when org deleted';
    END IF;
            
END;
$$ LANGUAGE plpgsql;

-- Market Tests
-- -----------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION TestMarket() RETURNS void AS $$
DECLARE
    marketId integer;
    count integer;
BEGIN
    INSERT INTO entity.market (status, name, schema_name) VALUES ('Active', 'Oahu', 'oahu');
    INSERT INTO entity.market (status, name, schema_name) VALUES ('Active', 'Lanai', 'lanai');
    SELECT COUNT(*) FROM entity.market INTO count;
    IF count <> 3       -- theres already a default market created
    THEN
        RAISE EXCEPTION 'Error in insert of market: % markets found', count;
    END IF;
    
    -- Test missing NOT NULL fields
    BEGIN    
        INSERT INTO entity.market (name)
            VALUES ('this_should_fail');
        EXCEPTION
            WHEN not_null_violation THEN    -- expected
            WHEN OTHERS THEN
                RAISE EXCEPTION 'Unexpected error raised: %, %', SQLERRM, SQLSTATE;
    END;

    -- Test incorrectly inserting two identical market names
    BEGIN
        INSERT INTO entity.market (status, name, schema_name) VALUES ('Active', 'Oahu', 'oahu');
        INSERT INTO entity.market (status, name, schema_name) VALUES ('Active', 'Oahu', 'oahu');
        EXCEPTION
            WHEN unique_violation THEN    -- expected
            WHEN OTHERS THEN
                RAISE EXCEPTION 'Unexpected error raised: %, %', SQLERRM, SQLSTATE;
        SELECT COUNT(*) FROM entity.market INTO count;
        IF count <> 1
        THEN 
            RAISE EXCEPTION 'Error in insert of market';
        END IF;
    END;
            
END;
$$ LANGUAGE plpgsql;

-- Organization-Market Tests
-- -----------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION TestOrganizationMarketBridge() RETURNS void AS $$
DECLARE
    marketId integer;
    orgId integer;
    count integer;
BEGIN
    INSERT INTO entity.market (status, name, schema_name) VALUES ('Active', 'Maui', 'maui') RETURNING market_id INTO marketId;
    INSERT INTO entity.organization (legal_name) VALUES ('Acme Greens') RETURNING org_id INTO orgId;
    INSERT INTO entity.organization_market VALUES (orgId, marketId);

    -- Test violation of foreign key constraints
    BEGIN
        INSERT INTO entity.organization_market VALUES (orgId + 2000001, marketId);
        EXCEPTION
            WHEN foreign_key_violation THEN -- expected 
            WHEN OTHERS THEN
                RAISE EXCEPTION 'Unexpected error raised: %, %', SQLERRM, SQLSTATE;
    END;
    BEGIN
        INSERT INTO entity.organization_market VALUES (orgId, marketId + 2000001);
        EXCEPTION
            WHEN foreign_key_violation THEN -- expected 
            WHEN OTHERS THEN
                RAISE EXCEPTION 'Unexpected error raised: %, %', SQLERRM, SQLSTATE;
    END;

    -- Test cascade delete constraints
    DELETE FROM entity.organization WHERE org_id = orgId;
    SELECT COUNT(*) FROM entity.organization_market WHERE org_id = orgId AND market_id = marketId INTO count;
    IF count > 0
    THEN
        RAISE EXCEPTION 'Error in Cascade delete of org from organization_market';
    END IF;
    INSERT INTO entity.organization (legal_name) VALUES ('Acme Greens') RETURNING org_id INTO orgId;
    INSERT INTO entity.organization_market VALUES (orgId, marketId);
    DELETE FROM entity.market WHERE market_id = marketId;
    SELECT COUNT(*) FROM entity.organization_market WHERE org_id = orgId AND market_id = marketId INTO count;
    IF count > 0
    THEN
        RAISE EXCEPTION 'Error in Cascade delete of market from organization_market';
    END IF;
    
END;
$$ LANGUAGE plpgsql;


-- -----------------------------------------------------------------------------
-- RUNNER
-- -----------------------------------------------------------------------------
DO $$ 
DECLARE
    userId integer;
    orgId integer;
BEGIN
    SELECT TestInsertUser() INTO userId;
    PERFORM TestInsertOrg();
    PERFORM TestCredentials(userId);
    PERFORM TestUserRole();
    PERFORM TestUserOrganizationBridge();
    PERFORM TestLocation();
    PERFORM TestMarket();
    PERFORM TestOrganizationMarketBridge();
END $$;

ROLLBACK;
--COMMIT;

