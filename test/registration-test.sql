-- Verify platform:registration-test on pg
/*
  Unit tests for the entity schema

  - registration: insert
  - registration_actions: insert
*/

SET client_min_messages = 'notice';

BEGIN;

-- Registration Insert Tests
-- -----------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION TestInsertRegistration() RETURNS uuid AS $$
DECLARE
    regUUID UUID;
BEGIN
    INSERT INTO entity.registration (status, username, ip_address, extended) 
        VALUES ('applied', 'foo@bar.com', '127.0.0.1', '{"foo":"bar"}') RETURNING reg_uuid INTO regUUID;

    IF regUUID IS NULL
    THEN
        RAISE EXCEPTION 'Registration UUID unexpectedly empty';
    END IF;
    RETURN regUUID;
END;
$$ LANGUAGE plpgsql;

-- Registration Action Tests
-- -----------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION TestInsertRegistrationActions(regUUID UUID) RETURNS void AS $$
DECLARE
    count integer;
BEGIN
    INSERT INTO entity.registration_action (reg_uuid, username, action, log) 
        VALUES (regUUID, 'foo@bar.com', 'email-sent', 'Sendmail processing log follows...');
    
    SELECT COUNT(*) FROM entity.registration_action WHERE reg_uuid = regUUID INTO count;
    IF count <> 1
    THEN
        RAISE EXCEPTION 'Error in insertion of registration actions';
    END IF;

END;
$$ LANGUAGE plpgsql;

-- -----------------------------------------------------------------------------
-- RUNNER
-- -----------------------------------------------------------------------------
DO $$ 
DECLARE
    regUUID UUID;
BEGIN
    SELECT TestInsertRegistration() INTO regUUID;
    PERFORM TestInsertRegistrationActions(regUUID);
END $$;

ROLLBACK;       -- Prevents polluting the database
--COMMIT;
